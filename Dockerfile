from alpine:3.10

run apk add --no-cache python3-dev \
    && pip3 install --upgrade pip 

workdir /app

copy . /app

run pip3 --no-cache-dir install -r requirements.txt

cmd ["python3", "src/app.py"]
