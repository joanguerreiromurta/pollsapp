from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

app = Flask(__name__)
#app.config['SQLALCHEMY_DATABASE_URI']='mysql+pymysql://asd:asd@host.docker.internal:3307/nubi' # Conexion fallida con el contenedor mysql
app.config['SQLALCHEMY_DATABASE_URI']='mysql+pymysql://root@localhost/nubi'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)

class Poll(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.String(255), unique = True)
    description = db.Column(db.String(255))

    def __init__(self, title, description):
        self.title = title
        self.description = description

db.create_all()

class PollSchema(ma.Schema):
    class Meta:
        fields = ('id', 'title', 'description')

poll_schema = PollSchema()
polls_schema = PollSchema(many = True)

@app.route('/polls', methods=['POST']) #Crea una encuesta
def create_poll():

    title = request.json['title']
    description = request.json['description']

    new_poll = Poll(title, description)
    db.session.add(new_poll)
    db.session.commit()

    return poll_schema.jsonify(new_poll)

@app.route('/polls', methods=['GET']) #Trae la lista de encuestas
def get_polls():
    
    all_polls = Poll.query.all()
    return polls_schema.jsonify(all_polls)

@app.route('/polls/<id>', methods=['GET']) #Trae una encuesta
def get_poll(id):

    poll = Poll.query.get(id)
    return poll_schema.jsonify(poll)

@app.route('/polls/<id>', methods=['PUT']) #Edita una encuesta
def update_poll(id):

    poll = Poll.query.get(id)

    title = request.json['title']
    description = request.json['description']

    poll.title = title
    poll.description = description

    db.session.commit()
    return poll_schema.jsonify(poll)

@app.route('/polls/<id>', methods=['DELETE']) #Elimina una encuesta
def delete_poll(id):

    poll = Poll.query.get(id)

    db.session.delete(poll)
    db.session.commit()

    return poll_schema.jsonify(poll)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)